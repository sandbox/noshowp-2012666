<?php

function media_wistia_global_settings()
{

  $form = array();

  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Installation'),
    '#collapsible' => FALSE,
    '#weight' => 10,
  );

  $status = media_flash_get_swfobject_status();

  $form['status']['report'] = array('#markup' => theme('status_report', array('requirements' => $status)));

  return $form;

}