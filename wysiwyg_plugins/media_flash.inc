<?php

function media_flash_media_flash_plugin() {

  $plugins['media_flash'] = array(
    'title' => 'Media Flash Fix',
//    'vendor url' => 'http://drupal.org/project/media',
//    'icon path' => drupal_get_path('module', 'media') . '/images',
//    'icon file' => 'wysiwyg-media.gif',
    'icon title' => '',
    // @todo: move this to the plugin directory for the wysiwyg plugin.
    'js path' => drupal_get_path('module', 'media_flash') . '/js',
    'js file' => 'wysiwyg-media_flash.js',
    'css file' => NULL,
    'css path' => NULL,
    'settings' => array(
      'global' => array(
//        'types' => media_variable_get('wysiwyg_allowed_types'),
        'id' => 'media_flash',
      ),
    ),
  );

  return $plugins;

}