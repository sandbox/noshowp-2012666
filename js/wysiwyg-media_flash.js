/**
 *  @file
 *  Attach Media WYSIWYG behaviors.
 */

(function ($) {

  Drupal.wysiwyg.plugins.media.originalForceAttributesIntoClass = Drupal.wysiwyg.plugins.media.forceAttributesIntoClass;

// Define the behavior.
  Drupal.wysiwyg.plugins.media.forceAttributesIntoClass = function (imgElement, fid, view_mode, additional) {

    this.originalForceAttributesIntoClass(imgElement, fid, view_mode, additional);
    var flash_vars = imgElement.attr('flash_vars');
    if (flash_vars) {
      imgElement.addClass('attr__flash_vars__' + flash_vars);
    }
  }

})(jQuery);
