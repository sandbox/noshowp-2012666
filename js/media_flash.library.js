(function ($) {
  namespace('Drupal.media.browser');

  Drupal.behaviors.flash = {
    attach: function (context, settings) {
      var library = new Drupal.media.browser.library(Drupal.settings.media.browser.flash);
      $('#media-browser-tabset').bind('tabsshow', function (event, ui) {
        if (ui.tab.hash === '#media-tab-flash') {
          // Grab the parameters from the Drupal.settings object
          var params = {};
          for (var parameter in Drupal.settings.media.browser.flash) {
            params[parameter] = Drupal.settings.media.browser.flash[parameter];
          }
          library.start($(ui.panel), params);
          $('#scrollbox').bind('scroll', library, library.scrollUpdater);
        }
      });
    }
  };

})(jQuery);