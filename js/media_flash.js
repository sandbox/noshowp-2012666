(function ($) {

  $(function () {

    $('.swfobject').each(function() {

      var flashvars = false,
        params = {
          flashvars: $(this).data('flash_vars')
        }
      swfobject.embedSWF(
        $(this).data('url'),
        $(this).attr('id'),
        $(this).data('width'),
        $(this).data('height'),
        "9.0.0", null, flashvars, params
      );
    })

  });

})(jQuery);