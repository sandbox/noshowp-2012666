<?php

/**
 * @file media_flash/includes/media_flash.formatters.inc
 *
 * Formatters for Media: Flash.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_flash_file_formatter_info()
{
  $formatters['media_flash_video'] = array(
    'label' => t('Swf Embed'),
    'file types' => array('flash'),
    'default settings' => array(),
    'view callback' => 'media_flash_file_formatter_swf_view',
    'settings callback' => 'media_flash_file_formatter_swf_settings',
  );

  foreach (media_flash_variable_default() as $setting => $value) {
    $formatters['media_flash_video']['default settings'][$setting] = media_flash_variable_get($setting);
  }

  // creating an image formatter so the swf isnt rendered in the media browser
  $formatters['media_flash_image'] = array(
    'label' => t('Flash Preview Image'),
    'file types' => array('flash'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_flash_file_formatter_image_view',
    'settings callback' => 'media_flash_file_formatter_image_settings',
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_flash_file_formatter_swf_view($file, $display, $langcode)
{
  // WYSIWYG does not yet support video inside a running editor instance.
  if (media_flash_is_swf($file->filename) && empty($file->override['wysiwyg'])) {
    $element = array(
      '#theme' => 'media_flash_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    foreach (media_flash_variable_default() as $setting => $value) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_flash_file_formatter_image_view($file, $display, $langcode) {
  // if we ever want an imager for the swf we can make it here
}

/**
 * @param $filename string
 * @return bool
 */
function media_flash_is_swf($filename)
{
  $needle = ".swf";
  $length = strlen($needle);
  return (substr($filename, -$length) === $needle);
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_flash_file_formatter_swf_settings($form, &$form_state, $settings)
{
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width']
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height']
  );
  $element['flash_vars'] = array(
    '#title' => t('Flash Vars'),
    '#type' => 'textfield',
    '#default_value' => $settings['flash_vars']
  );

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_flash_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );
  return $element;
}

/**
 * Implements hook_file_default_displays().
 */
function media_flash_file_default_displays()
{

  // Default settings for displaying as a video.
  $default_displays = array();

  $default_video_settings = array(
    'default' => array(
      'width' => 640,
      'height' => 390,
    ),
    'teaser' => array(
      'width' => 560,
      'height' => 340
    ),
    // Legacy view modes included to support older versions of Media,
    // before #1051090 went through. They do no harm and can sit here
    // until there is a Media 2.x stable.
    // @TODO: Remove when Media 2.0 is released.
    'media_large' => array(
      'width' => 560,
      'height' => 340
    ),
    'media_original' => array(
      'width' => 640,
      'height' => 390
    ),
  );
  foreach ($default_video_settings as $view_mode => $settings) {
    $display_name = 'flash__' . $view_mode . '__media_flash_video';
    $default_displays[$display_name] = (object)array(
      'api_version' => 1,
      'name' => $display_name,
      'status' => 1,
      'weight' => 1,
      'settings' => $settings,
    );
  }

  // Default settings for displaying a video preview image.
  // We enable preview images even for view modes that also play video
  // for use inside a running WYSIWYG editor. We weight them so video
  // formatters come first in the cascade to make sure the video formatter
  // is used whenever possible.
  $default_image_styles = array(
    'default' => 'large',
    'preview' => 'square_thumbnail',
    'teaser' => 'large',
    // Legacy view modes, see note above.
    'media_preview' => 'square_thumbnail',
    'media_large' => 'large',
    'media_original' => '',
  );
  foreach ($default_image_styles as $view_mode => $image_style) {
    $display_name = 'flash__' . $view_mode . '__media_flash_image';
    $default_displays[$display_name] = (object) array(
      'api_version' => 1,
      'name' => $display_name,
      'status' => 1,
      'weight' => 2,
      'settings' => array('image_style' => $image_style),
    );
  }

  return $default_displays;
}
