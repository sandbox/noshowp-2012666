<?php

/**
 * @file media_flash/includes/themes/media-flash.tpl.php
 *
 * Template file for theme('media_flash_video').
 *
 * Variables available:
 *
 */

?>
<div class="<?php print $classes; ?> media-flash-<?php print $id; ?>">

  <div id="flash-<?php print $id; ?>" class="swfobject" data-width="<?php echo $width ?>" data-height="<?php echo $height ?>" data-url="<?php echo $url ?>" data-flash_vars="<?php echo $flash_vars ?>">
    <span>Alternative content</span>
  </div>

</div>