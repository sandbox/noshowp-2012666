<?php

/**
 * @file media_flash/includes/themes/media_flash.theme.inc
 *
 * Theme and preprocess functions for Media: Flash.
 */

/**
 * Preprocess function for theme('media_flash_video').
 */
function media_flash_preprocess_media_flash_video(&$variables) {

  // Add some options as their own template variables.
  foreach (array('width', 'height', 'flash_vars') as $theme_var) {
    $variables[$theme_var] = $variables['options'][$theme_var];
  }

  $variables['url'] = file_create_url($variables['uri']);
}